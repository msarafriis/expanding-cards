# expanding-cards

This is a visual effect that makes pages look cooler.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).

Images (c) Apple
